package com.isoft.internship.car.utility;

import com.isoft.internship.car.model.Audi;
import com.isoft.internship.car.model.BMW;
import com.isoft.internship.car.model.Car;
import com.isoft.internship.car.model.Volkswagen;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private List<Car> carList = new ArrayList<>();
    private Scanner scanner = new Scanner(System.in);
    public void run(){
        Car audi = new Audi(2008,"Green","A6");
        Car bmw = new BMW(2010,"Black","E90");
        Car volkswagen = new Volkswagen(2005,"Blue","Passat");
        carList.add(audi);
        carList.add(bmw);
        carList.add(volkswagen);
        System.out.println("1: Show the cars");
        System.out.println("2: Exit");
        handleInput(scanner.nextInt());

    }

    public void handleInput(Integer choice){
        switch (choice) {
            case 1:
                for(Car car : carList)
                    System.out.println(car.toString());
                run();
                break;

            case 2:
                System.exit(0);
                break;
            default:
                System.out.println("\nInvalid input!");
                run();
        }

        }
}
