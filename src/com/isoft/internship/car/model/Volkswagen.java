package com.isoft.internship.car.model;

public class Volkswagen extends Car {
    private final String make = "Volkswagen";
    private String model;

    public Volkswagen(int year, String color, String model) {
        super(year, color);
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Volkswagen{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                "} " + super.toString();
    }
}
