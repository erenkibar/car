package com.isoft.internship.car.model;

public class BMW extends Car {
    private final String make = "BMW";
    private String model;

    public BMW(int year, String color, String model) {
        super(year, color);
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "BMW{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                "} " + super.toString();
    }
}
