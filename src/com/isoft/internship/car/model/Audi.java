package com.isoft.internship.car.model;

public class Audi extends Car {
    private final String make = "Audi";
    private String model;

    public Audi(int year, String color, String model) {
        super(year, color);
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Audi{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                "} " + super.toString();
    }
}
