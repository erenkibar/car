package com.isoft.internship.car;

import com.isoft.internship.car.utility.Menu;

public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.run();
    }
}
